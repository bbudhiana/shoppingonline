
## About ShoppingOnline

This application is an online shopping app using the latest codeigniter framework version 3.15. This shopping site has also been equipped with facilities for payment using stripe, ie payment by credit card through payment gateway. 

## Prerequisite
	
- Your site using PHP with minimum version 5.6, this program has been tested on php 7.x and running well
- You have install composer (https://getcomposer.org)
- You have install npm and bower, component desain using this application to get the design packages with boostrap, jquery, etc.
- You understand to enable rwrite or deflate module on apache, shopping online using pretty url.
- You have register at stripe.com and have the key needed in config/stripe.php

## Features
1. Authentication base on tank auth (tank auth have modified by me)
	- Login
	- Register
	- Activation
	- Banned
	- Forgot Password
	- Captcha
	- etc

2.	Shopping 
	- Product storefront
	- Stripe payment gateway, you just insert your key in config
	- Cek your order payment status
	- Product Management (for Admin)
	- Order Management (for Admin)
	- User Management (for Admin)

## Installation
- git clone https://bbudhiana@bitbucket.org/bbudhiana/shoppingonline.git
- go to the root system, example : cd shoppingonline
- run composer install : $ composer install
- run bower            : $ bower install
- create database in your server or system and dont forget give it a credential
- open your config/database.php and change configuration 
- open your phinx.yml and change base on your database configuration
- open your config/stripe.php and put your stripe key there
- run following command :
	- $ vendor/bin/phinx rollback -e development -t 0
    - $ vendor/bin/phinx migrate -e development
	- $ vendor/bin/phinx seed:run -e development
	
- Above command will create table
- Give write permission for this folder : captcha, assets/uploads

## If You Succeded Installation
This Shopping Online have default user:
- Admin, with login 'admin@gmail.com' and password 'admin123' for Admin Role
- Johny, with login 'johny@gmail.com' and password 'johny123' for User Role


## License

The Shopping Online is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).