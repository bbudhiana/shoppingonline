<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class AuthMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table_session = $this->table('ci_sessions', array('id'=>false));
        $table_session->addColumn('id', 'string', ['limit'=>128, 'null'=>false])
                      ->addColumn('ip_address', 'string', ['limit'=>45, 'null'=>false])
                      ->addColumn('timestamp', 'integer', ['limit'=>10, 'signed'=>false, 'null'=>false, 'default'=>0])
                      ->addColumn('data', 'blob', ['null'=>false])
                      ->addIndex(['timestamp'], ['unique' => false, 'name' => 'ci_sessions_timestamp'])
                      ->create();

        $table_login_attempts = $this->table('login_attempts');
        $table_login_attempts->addColumn('ip_address', 'string', ['limit'=>150, 'null'=>false])
                             ->addColumn('login', 'string', ['limit'=>50, 'null'=>false])
                             ->addColumn('time', 'timestamp', ['null'=>false, 'default' => 'CURRENT_TIMESTAMP', 'update'=> 'CURRENT_TIMESTAMP'])
                             ->create();

         $table_users = $this->table('users');
         $table_users->addColumn('username', 'string', ['limit'=>50, 'null'=>false])
                     ->addColumn('password', 'string', ['limit'=>255, 'null'=>false])
                     ->addColumn('email', 'string', ['limit'=>100, 'null'=>false])
                     ->addColumn('role', 'enum', ['null'=>false, 'values'=>['Admin', 'User'], 'default'=>'User'])
                     ->addColumn('activated', 'integer', ['limit' => MysqlAdapter::INT_TINY, 'null'=>false, 'default'=>1])
                     ->addColumn('banned', 'integer', ['limit' => MysqlAdapter::INT_TINY, 'null'=>false, 'default'=>0])
                     ->addColumn('ban_reason', 'string', ['limit'=>255, 'null'=>true])
                     ->addColumn('new_password_key', 'string', ['limit'=>50, 'null'=>true])
                     ->addColumn('new_password_requested', 'datetime', ['limit'=>50, 'null'=>true])
                     ->addColumn('new_email', 'string', ['limit'=>100, 'null'=>true])
                     ->addColumn('new_email_key', 'string', ['limit'=>50, 'null'=>true])
                     ->addColumn('last_ip', 'string', ['limit'=>40, 'null'=>false])
                     ->addColumn('last_login', 'datetime', ['null'=>false, 'default'=>'1970-01-01 00:00:01'])
                     ->addColumn('created', 'datetime', ['null'=>false, 'default'=>'1970-01-01 00:00:01'])
                     ->addColumn('modified', 'timestamp', ['null'=>false, 'default'=>'CURRENT_TIMESTAMP', 'update'=> 'CURRENT_TIMESTAMP'])
                     ->addColumn('stripe_customer_id', 'string', ['limit'=>50, 'null'=>true])
                     ->create();

        $table_user_autologin = $this->table('user_autologin', array('id'=>false, 'primary_key'=>['key_id', 'user_id']));
        $table_user_autologin->addColumn('key_id', 'char', ['limit'=>32, 'null'=>false])
                             ->addColumn('user_id', 'integer', ['limit'=>11, 'null'=>false, 'default'=>0])
                             ->addColumn('user_agent', 'string', ['limit'=>150, 'null'=>false])
                             ->addColumn('last_ip', 'string', ['limit'=>150, 'null'=>false])
                             ->addColumn('last_login', 'timestamp', ['null'=>false, 'default'=>'CURRENT_TIMESTAMP', 'update'=> 'CURRENT_TIMESTAMP'])
                             ->create();

        $table_user_profiles = $this->table('user_profiles');
        $table_user_profiles->addColumn('user_id', 'integer', ['limit'=>11, 'null'=>false])
                            ->addColumn('fullname', 'string', ['limit'=>150, 'null'=>true])
                            ->addColumn('country', 'string', ['limit'=>150, 'null'=>true])
                            ->addColumn('website', 'string', ['limit'=>250, 'null'=>true])
                            ->addColumn('address', 'string', ['limit'=>250, 'null'=>true])
                            ->create();



    }
}
