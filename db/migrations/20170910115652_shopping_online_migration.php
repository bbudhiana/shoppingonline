<?php

use Phinx\Migration\AbstractMigration;

class ShoppingOnlineMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table_orders = $this->table('orders', ['id' => 'serial']);
        //$table_orders->addColumn('serial', 'integer', ['limit'=>11, 'null'=>false])
        $table_orders->addColumn('date', 'date', ['null'=>false])
                     ->addColumn('user_id', 'integer', ['limit'=>11, 'null'=>false])
                     ->addColumn('fullname', 'string', ['limit'=>150, 'null'=>true])
                     ->addColumn('address', 'string', ['limit'=>250, 'null'=>true])
                     ->addColumn('created', 'datetime', ['default'=>'1970-01-01 00:00:01', 'null'=>false])
                     ->addColumn('stripe_charge_id', 'string', ['limit'=>150, 'null'=>true])
                     ->addColumn('message', 'string', ['limit'=>150, 'null'=>true])
                     ->create();

        $table_order_detail = $this->table('order_detail', ['id' => false]);
        $table_order_detail->addColumn('orderid', 'integer', ['limit'=>11, 'null'=>false])
                           ->addColumn('productid', 'integer', ['limit'=>11, 'null'=>false])
                           ->addColumn('quantity', 'integer', ['limit'=>11, 'null'=>false])
                           ->addColumn('price', 'float', ['null'=>false])
                           ->create();

        $table_products = $this->table('products', ['id' => 'serial']);
        $table_products->addColumn('name', 'string', ['limit'=>120, 'null'=>false])
                        ->addColumn('description', 'string', ['limit'=>255, 'null'=>false])
                        ->addColumn('price', 'float', ['null'=>false])
                        ->addColumn('picture', 'string', ['limit'=>100, 'null'=>true])
                        ->addColumn('picture_ext', 'string', ['limit'=>100, 'null'=>true])
                        ->addColumn('stock', 'integer', ['limit'=>10, 'null'=>true, 'default'=>0, 'signed'=>false])
                        ->create();
    }
}
