<?php

use Phinx\Seed\AbstractSeed;

class ProductSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
         $data = [
            [
                'name'              => 'Google Nexus 6',
                'description'       => 'The Qualecomm @ snapdragon 805 quard-core processor provides lightning fast multi tasking and the Adreno 420 Gpu gives you brilliant graphics.',
                'price'             =>  699,
                'picture'           => 'sample2_l',
                'picture_ext'       => '.jpg',
                'stock'             => '12',
            ],
            [
                'name'              => 'Home Theater',
                'description'       => 'Home Theater 4.1 with USB and FM Wired Home Audio Speaker',
                'price'             =>  500,
                'picture'           => 'sample5_l',
                'picture_ext'       => '.jpg',
                'stock'             => '17',
            ],
            [
                'name'              => 'Samsung Split AC',
                'description'       => 'Out Door Unit Stand. Extra copper wire if any. Drain Pipe extension if any. Plumbing and Masonry Work. Wiring extension from Meter to site, Power point/MCB fitting and any other electrical work,Carpentry work.',
                'price'             =>  300,
                'picture'           => 'sample4_l',
                'picture_ext'       => '.jpg',
                'stock'             => '13',
            ],
            [
                'name'              => 'Nikon DSLR Camera',
                'description'       => 'Black, Body with AF-S DX NIKKOR 18-55mm f/3.5-5.6G VR II Lens) 2 Years Nikon India Warranty and Free Transit Insurance.',
                'price'             =>  800,
                'picture'           => 'sample4_l',
                'picture_ext'       => '.jpg',
                'stock'             => '13',
            ],
            [
                'name'              => 'Tea maker',
                'description'       => 'Morphy Richards 1.5 Ltr - Tea Maker Silver Black',
                'price'             =>  100,
                'picture'           => 'sample4_l',
                'picture_ext'       => '.jpg',
                'stock'             => '13',
            ],
            [
                'name'              => 'LG 808990',
                'description'       => 'This Celular Phone from LG that have 4 core processor and 16GB RAM',
                'price'             =>  220,
                'picture'           => 'sample4_l',
                'picture_ext'       => '.jpg',
                'stock'             => '9',
            ]
        ];

        $posts = $this->table('products');
        $posts->insert($data)
              ->save();
    }
}
