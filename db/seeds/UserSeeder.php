<?php

use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'username'    => 'admin',
                'password'    => password_hash('admin123', PASSWORD_DEFAULT),
                'email'       => 'admin@gmail.com',
                'role'        => 'Admin',
                'activated'   => 1,
                'banned'      => 0,
                'last_ip'     => '::1',
                'last_login'  => date('Y-m-d H:i:s'),
                'created'     => date('Y-m-d H:i:s'),
                'modified'    => date('Y-m-d H:i:s'),
            ],
            [
                'username'    => 'johny',
                'password'    => password_hash('johny123', PASSWORD_DEFAULT),
                'email'       => 'johny@gmail.com',
                'role'        => 'User',
                'activated'   => 1,
                'banned'      => 0,
                'last_ip'     => '::1',
                'last_login'  => date('Y-m-d H:i:s'),
                'created'     => date('Y-m-d H:i:s'),
                'modified'    => date('Y-m-d H:i:s'),
            ]
        ];

        $posts = $this->table('users');
        $posts->insert($data)
              ->save();
    }
}
