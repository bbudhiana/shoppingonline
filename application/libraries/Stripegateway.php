<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once('vendor/autoload.php');

/**
 * Stripegateway
 *
 * Stripe Gateway library for Code Igniter.
 *
 * @package		stripe
 * @author		Bana Budhiana
 * @version		1.0.0
 * @license		MIT License Copyright (c) 2017 Bana Budhiana
 */

class Stripegateway {

	/**
	 * $charge_id is variable for storing charge_id from stripe when we do payment with CC
	 * @var string
	 */
	protected $charge_id;

	/**
	 * $ci is instant CI Core
	 * @var object
	 */
	protected $ci;
  
    public function __construct() 
    {	
    	$this->ci =& get_instance();

    	$this->ci->load->config('stripe', TRUE);
	 
	 	\Stripe\Stripe::setApiKey($this->ci->config->item('secret_key', 'stripe'));
   }
	
	/**
	 * Charge base on data array. Return 'succeeded' if Stripe is successful
	 * otherwise String Error by exception.
	 *
	 * @param	array (amount, description, order_id, customer_id)
	 * @return	string
	 */
  	public function charge($data) 
  	{
	    $message = '';
	    try {

			$charge = \Stripe\Charge::create(array(
				//"card" => $mycard,
			  	"amount" => $data['amount'],
			  	"currency" => "usd",
			  	"description" => $data['description'],
			  	"metadata" => array('order_id' => $data['order_id']),
			  	'customer' => $data['customer_id']
			  	//"source" => $token,
			));
			
			$this->charge_id = $charge->id;
			$message = $charge->status;
			
	    } catch(Exception $e) {
		   $message = $e->getMessage();
	    }
	   return $message;
  	}

  	/**
  	 * create stripe customer base on email and token
  	 * @param  string $email email customer that want to register in stripe account
  	 * @param  string $token random number create from stripe
  	 * @return json        
  	 */
  	public function create_stripe_customer($email, $token)
  	{
  		$customer = \Stripe\Customer::create(array(
			  'email' => $email,
			  'source'  => $token
		));

		return $customer;
  	}

  	/**
  	 * get_charge_id is function for get stripe charge_id
  	 * @return [type] [description]
  	 */
  	public function get_charge_id()
  	{
  		return $this->charge_id;
  	}

  	/**
  	 * get_charge_info is function for get info from stripe for every transaction using stripe
  	 * @param  [type] $charge_id [description]
  	 * @return [type]            [description]
  	 */
    public function get_charge_info($charge_id)
    {
   		//return \Stripe\Charge::retrieve("ch_1AzQzyDJ8ODN8boeJ34Y8TC5");
   		return \Stripe\Charge::retrieve($charge_id);
    }
}
