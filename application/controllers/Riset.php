<?php

class Test extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('asset');
		$this->load->helper('url');
		$this->load->helper('asset');
		$this->load->database();
	}

	public function index()
	{
		echo 'Hello Gan';
	}

	public function comments()
	{
		echo 'this is comments page';
	}

	public function shoes($sandals, $id)
	{
		echo $sandals.' with id: '.$id;
	}

	public function testsomefunction()
	{
		echo APPPATH;
		echo CI_VERSION;
		echo ENVIRONMENT;
	}

	public function testgetconfig()
	{
		$this->config->load('stripe');
		echo $this->config->item('secret_key');
	}

	public function testtemplate()
	{
		//var_dump(asset_url()); exit();
		$this->load->view('templates/alltemplate');
	}

	public function testambilconfig()
	{
		//$this->output->enable_profiler(TRUE);
		$this->config->load('tank_auth', TRUE);
		echo $this->config->item('website_name', 'tank_auth');
	}

	public function testupload()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->view('templates/testupload', array('error'=>''));
	}

	public function do_upload()
    {
            $config['upload_path']          = upload_path();
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 1000;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $config['override']             = TRUE;

            $this->load->helper(array('form', 'url'));
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('userfile'))
            {
                    $error = array('error' => $this->upload->display_errors());

                    $this->load->view('templates/testupload', $error);
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());

                    $this->create_thumb($data['upload_data']['full_path']);

                    $this->load->view('templates/testuploadsuccess', $data);
            }
    }

    public function create_thumb($file)
    {
    	$config['image_library'] = 'gd2';
		$config['source_image'] = $file;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 175;
		$config['height']       = 150;
		//$config['thumb_marker'] = '_thumb';

		$this->load->library('image_lib', $config);

		if ( ! $this->image_lib->resize())
		{
		    echo $this->image_lib->display_errors();
		}
    }

    public function testdb()
    {
    	$this->db->where('price >', 400);
    	//$this->db->like('name', 'Google', 'both');   // like "%string%"
    	//$this->db->like('name', 'Google', 'before'); // like "%string"
    	$query = $this->db->get('products');
    	//$sql = $this->db->get_compiled_select('products');
    	//var_dump($query->result());
    	//
    	$this->load->model('billing_model');
    	var_dump($this->billing_model->get_all());
    }
}