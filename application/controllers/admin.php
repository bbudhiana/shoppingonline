<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->config->load('asset');
		$this->load->helper(array('form', 'security', 'url', 'asset'));
		$this->load->helper(array('url', 'asset'));

		$this->load->model('tank_auth/users');
		$this->load->model('billing_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->tank_auth->is_logged_in()) {									// logged in

			$this->load->library('pagination');

			/* This Application Must Be Used With BootStrap 3 *  */
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] ="</ul>";
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";
	        //send all product with paging
	        $config['base_url']= base_url().'/admin/users';
	        $config['total_rows'] = $this->billing_model->get_count_users();
	        $config['per_page'] = 15;
	        $config['use_page_numbers'] = TRUE;
	       	
	        $from = $this->uri->segment(3)==NULL?0:($this->uri->segment(3)*$config['per_page'])-$config['per_page'];

	        $this->pagination->initialize($config);

			$users = $this->billing_model->get_all_users_pagination($config['per_page'], $from);

			$data['users']  = $users;
			$data['title'] = 'Users	';
			$this->load->view('templates/header', $data);
			$this->load->view('admin/users', $data);
			$this->load->view('templates/footer');

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/auth/send_again/');
		} else {
			redirect('/auth/login');
		}
	}

	public function remove_user()
	{
		$user_id = $this->uri->segment(3);

		if($this->tank_auth->get_user_id() != $user_id){
			$this->tank_auth->delete_user_by_id($user_id)?
			$this->session->set_flashdata('message', array('type'=>'success','message'=>'Successfully delete the user!')):
			$this->session->set_flashdata('message', array('type'=>'alert','message'=>'Something goes wrong!'));
		} else {
			$this->session->set_flashdata('message', array('type'=>'warning','message'=>'You cannot delete your self!'));
		}

		redirect('admin');
	}

	public function edit_user()
	{
		$user_id = $this->uri->segment(3);

		$user = $this->billing_model->get_user($user_id);

		if($this->input->post('submit')){
			$this->tank_auth->update_user_without_password($user_id, 
				array('activated'=>$this->input->post('activated'), 
					  'role'=>$this->input->post('role')));
			$this->session->set_flashdata('message', array('type'=>'success','message'=>'Data User telah diubah!'));
			redirect('admin');
		}

		$data['user'] = $user;
		$data['title'] = 'Edit User';

		$this->load->view('templates/header', $data);
		$this->load->view('admin/user_form', $data);
		$this->load->view('templates/footer');
	}

	public function products()
	{
		if ($this->tank_auth->is_logged_in()) {									// logged in

			$this->load->library('pagination');

			/* This Application Must Be Used With BootStrap 3 *  */
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] ="</ul>";
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";
	        //send all product with paging
	        $config['base_url']= base_url().'/admin/products';
	        $config['total_rows'] = $this->billing_model->get_count_products();
	        $config['per_page'] = 15;
	        $config['use_page_numbers'] = TRUE;
	       	
	        $from = $this->uri->segment(3)==NULL?0:($this->uri->segment(3)*$config['per_page'])-$config['per_page'];

	        $this->pagination->initialize($config);

			$products = $this->billing_model->get_all_products_pagination($config['per_page'], $from);

			$data['products']  = $products;
			$data['title'] = 'Products	';
			$this->load->view('templates/header', $data);
			$this->load->view('admin/products', $data);
			$this->load->view('templates/footer');

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/auth/send_again/');
		} else {
			redirect('/auth/login');
		}
	}

	public function remove_product()
	{
		$product_id = $this->uri->segment(3);

		if($this->tank_auth->get_user_id() != $user_id){
			$this->billing_model->delete_product($product_id);
			$this->session->set_flashdata('message', array('type'=>'succces','message'=>'Delete product successfully!'));
		} else {
			$this->session->set_flashdata('message', array('type'=>'warning','message'=>'You cannot delete your self!'));
		}

		redirect('admin/products');
	}

	public function edit_product()
	{
		$product_id = $this->uri->segment(3);

		$product = $this->billing_model->get_product($product_id);

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim|required');
		$this->form_validation->set_rules('price', 'Price', 'trim|numeric|required');
		$this->form_validation->set_rules('stock', 'Stock', 'trim|numeric|required');

		if ($this->form_validation->run()) {   										// if validation ok
			$data_product = array(
				'name' => $this->input->post('name'),
				'description'  => $this->input->post('description'),
				'price' => $this->input->post('price'),
				'stock' => $this->input->post('stock')

			);
			if($this->input->post('productid')) {
				$product_id =  $this->input->post('productid');
				$this->billing_model->update_product(									// update product
					$this->input->post('productid'), $data_product
				);
				$this->session->set_flashdata('message', array('type'=>'success','message'=>'Edit product successfully!'));								
			}else{
				$product_id = $this->billing_model->insert_product($data_product);                   // add product	
				$this->session->set_flashdata('message', array('type'=>'success','message'=>'Add product successfully!'));	
			}

			if (isset($_FILES['userFile']['name']) && !empty($_FILES['userFile']['name'])) {
				$this->do_upload($product_id);
			}
			
			redirect('/admin/products');					// After storing all imformation in database load "billing_success".
		}

		$data['product'] = $product;
		$data['title'] = 'Edit Product';

		$this->load->view('templates/header', $data);
		$this->load->view('admin/product_form', $data);
		$this->load->view('templates/footer');
	}


	public function do_upload($productid)
    {
            $config['upload_path']          = upload_path();
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 1000;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $config['override']             = TRUE;

            $this->load->helper(array('form', 'url'));
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('userFile'))
            {
            	//var_dump($this->upload->display_errors());exit();
            	$this->session->set_flashdata('message', array('type'=>'warning','message'=>'Upload Failed'));
                return FALSE;
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
                    $this->create_thumb($data['upload_data']['full_path']);
                    $this->billing_model->update_product(									// update product
						$productid, array('picture'=>$data['upload_data']['raw_name'],
										  'picture_ext'=>$data['upload_data']['file_ext']
										)
					);
                    return TRUE;
            }
    }

    public function create_thumb($file)
    {
    	$config['image_library'] = 'gd2';
		$config['source_image'] = $file;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 200;
		$config['height']       = 200;
		$config['thumb_marker'] = '_thumb';

		$this->load->library('image_lib', $config);

		if ( ! $this->image_lib->resize())
		{
		    echo $this->image_lib->display_errors();
		}
    }


    public function orders()
    {
    	$this->load->library('pagination');
		//load stripe payment gateway
		$this->load->library('stripegateway');

		/* This Application Must Be Used With BootStrap 3 *  */
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
        //send all product with paging
        $config['base_url']= base_url().'/admin/orders';
        $config['total_rows'] = $this->billing_model->get_count_orders();
        $config['per_page'] = 9;
        $config['use_page_numbers'] = TRUE;
       	
        $from = $this->uri->segment(3)==NULL?0:($this->uri->segment(3)*$config['per_page'])-$config['per_page'];

        $this->pagination->initialize($config);

        $data['orders'] = $this->billing_model->get_all_orders_pagination( $config['per_page'], $from);

		$data['title'] = ucfirst('All Orders');
		$this->load->view('templates/header', $data);
		$this->load->view('admin/orders', $data);
		$this->load->view('templates/footer');
    }
}