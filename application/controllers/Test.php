<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('unit_test');
		$this->load->model('billing_model');
	}

	public function test_get_products()
	{
		$test = $this->billing_model->get_all();
		$expected_result = 'is_array';
		$test_name = 'Get All Products for Home Website';

		$this->unit->run($test, $expected_result, $test_name);
	}

	public function test_get_order_by_id()
	{	
		$order_id = 1;
		$test = $this->billing_model->get_order($order_id);
		$expected_result = 'is_object';
		$test_name = 'Get Order By ID for Pay';

		$this->unit->run($test, $expected_result, $test_name);
	}

	public function test_get_count_all_order()
	{
		$test = $this->billing_model->get_count_order();
		$expected_result = 'is_numeric';
		$test_name = 'Get Count All Order';

		$this->unit->run($test, $expected_result, $test_name);
	}

	public function test_all()
	{	
		$this->test_get_products();
		$this->test_get_order_by_id();
		$this->test_get_count_all_order();
		echo $this->unit->report();
	}
}