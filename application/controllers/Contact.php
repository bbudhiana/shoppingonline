<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->config->load('asset');
		$this->load->helper(array('form', 'url', 'asset'));
		$this->load->helper('asset');
	}

	public function index()
	{
		$data['title'] = 'About';
		$this->load->view('templates/header', $data);
		$this->load->view('pages/contact', $data);
		$this->load->view('templates/footer');
	}

	public function me()
	{
		echo 'this is me';
	}
}