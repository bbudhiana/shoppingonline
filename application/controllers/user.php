<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->config->load('asset');
		$this->load->helper(array('form', 'security', 'url', 'asset'));
		$this->load->helper(array('url', 'asset'));

		$this->load->model('tank_auth/users');
		$this->load->model('billing_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->tank_auth->is_logged_in()) {									// logged in

			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');

			if ($this->form_validation->run()) {								// validation ok
				$this->tank_auth->update_user($this->tank_auth->get_user_id(), array('password'=>$this->form_validation->set_value('password')));
				$this->session->set_flashdata('message', array('type'=>'success','message'=>'Data Account telah diubah!'));
				redirect('user');
			}

			$user = $this->billing_model->get_user($this->tank_auth->get_user_id());

			$data['user']  = $user;
			$data['title'] = 'Account';
			$this->load->view('templates/header', $data);
			$this->load->view('user/account', $data);
			$this->load->view('templates/footer');

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/auth/send_again/');
		} else {
			redirect('/auth/login');
		}
	}

	public function profile()
	{
		$user = $this->billing_model->get_user($this->tank_auth->get_user_id());
		$user_profile = $this->billing_model->get_user_profile($this->tank_auth->get_user_id());

		if($this->input->post('submit')){
			$this->tank_auth->update_user_profile($this->tank_auth->get_user_id(), array('fullname'=>$this->input->post('fullname'), 'address'=>$this->input->post('address')));
			$this->session->set_flashdata('message', array('type'=>'success','message'=>'Data Profile telah diubah!'));
			redirect('user/profile');
		}

		$data['user']  = $user;
		$data['user_profile']  = $user_profile;
		$data['title'] = 'Account';
		$this->load->view('templates/header', $data);
		$this->load->view('user/profile', $data);
		$this->load->view('templates/footer');
	}

	public function orders()
	{
		$this->load->library('pagination');
		//load stripe payment gateway
		$this->load->library('stripegateway');

		/* This Application Must Be Used With BootStrap 3 *  */
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
        //send all product with paging
        $config['base_url']= base_url().'/user/orders';
        $config['total_rows'] = $this->billing_model->get_count_user_orders($this->tank_auth->get_user_id());
        $config['per_page'] = 9;
        $config['use_page_numbers'] = TRUE;
       	
        $from = $this->uri->segment(3)==NULL?0:($this->uri->segment(3)*$config['per_page'])-$config['per_page'];

        $this->pagination->initialize($config);

        $data['orders'] = $this->billing_model->get_all_user_orders_pagination($this->tank_auth->get_user_id(), $config['per_page'], $from);

		$data['title'] = ucfirst('Orders');
		$this->load->view('templates/header', $data);
		$this->load->view('user/orders', $data);
		$this->load->view('templates/footer');
	}

}