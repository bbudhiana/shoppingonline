<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shopping extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->config->load('asset');
		$this->load->helper(array('form', 'security', 'url', 'asset'));
		$this->load->helper(array('url', 'asset'));

		$this->load->model('billing_model');
		$this->load->model('tank_auth/users');
		$this->load->library('form_validation');
        $this->load->library('cart');
        $this->load->library('tank_auth');
		$this->lang->load('tank_auth');
	}

	public function index()
	{	
		$this->load->library('pagination');
        //Get all data from database
		//$data['products'] = $this->billing_model->get_all();
        //send all product data to "shopping_view", which fetch from database.		
        
        //set bootstrap paging
        /* This Application Must Be Used With BootStrap 3 *  */
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
        //send all product with paging
        $config['base_url']= base_url().'/shopping';
        $config['total_rows'] = $this->billing_model->get_count_products();
        $config['per_page'] = 9;
        $config['use_page_numbers'] = TRUE;
        $from = $this->uri->segment(2)==NULL?0:($this->uri->segment(2)*$config['per_page'])-$config['per_page'];

        $this->pagination->initialize($config);
        $data['products'] = $this->billing_model->get_all_products_pagination($config['per_page'], $from);
		
		$data['title'] = ucfirst("Products List");

		$this->load->view('templates/header', $data);
		$this->load->view('shopping', $data);
		$this->load->view('templates/footer', $data);
	}

	public function cart()
	{
		$data['title'] = ucfirst("Shopping Cart");

		$this->load->view('templates/header', $data);
		$this->load->view('cart', $data);
		$this->load->view('templates/footer', $data);
	}

	public function billing()
	{
		if ($this->tank_auth->is_logged_in()) {									// logged in
			if (empty($this->cart->contents())) redirect('shopping/');			// if cart empty then back to shopping page

			//define validation rule
			$this->form_validation->set_rules('fullname', 'Fullname', 'trim|required');
			$this->form_validation->set_rules('address', 'Address', 'trim|required');

			if ($this->form_validation->run()) {   								// if validation ok
				$this->users->update_profile(									// save address and fullname
					$this->input->post('user_id'),
					array(
						'fullname' => $this->input->post('fullname'),
						'address'  => $this->input->post('address'),
					)
				);
				$ord_id = $this->save_order();									// do save order and proses a payment
				$this->session->set_flashdata('order_id', $ord_id);
				redirect('/shopping/billing_success');					// After storing all imformation in database load "billing_success".
			}

			$data['title'] = ucfirst("Checkout");

			$this->load->view('templates/header', $data);
			$this->load->view('checkout', $data);
			$this->load->view('templates/footer', $data);

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/auth/send_again/');

		} else { 
		   	$this->session->set_flashdata('message', array('type'=>'warning','message'=>'You must login to pay your cart!'));

		   	redirect('/auth/login');
		}
	}

	public function billing_success()
	{
		if(!$this->session->userdata('order_id')) redirect('shopping');
		$data['title'] = ucfirst("Billing Success");

		//$ord_id = $this->uri->segment(3);
		$ord_id = $this->session->flashdata('order_id');
		$order = $this->billing_model->get_order($ord_id);
		$order_detail = $this->billing_model->get_order_detail($ord_id);
		$data['order'] = $order;
		$data['order_detail'] = $order_detail;

		$this->load->view('templates/header', $data);
		$this->load->view('billing_success', $data);
		$this->load->view('templates/footer', $data);
	}

	public function add()
	{
		//create array cart item for cart input data
		$insert_data = array(
			'id' => $this->input->post('id'),
			'name' => $this->input->post('name'),
			'price' => $this->input->post('price'),
			'qty' => (int)$this->input->post('qty')
		);

		// This function add items into cart.
		$this->cart->insert($insert_data);

		$this->session->set_flashdata('message', array('type'=>'success','message'=>'product has been add to your cart'));
		// This will show insert data in cart.
		redirect('shopping');
	}

	public function remove($rowid) 
	{
		// Check rowid value.
		if ($rowid==="all"){
			// Destroy data which store in session.
			$this->cart->destroy();
		}else{
			// Destroy selected rowid in session.
			$data = array(
				'rowid' => $rowid,
				'qty' => 0
			);
			// Update cart data, after cancel.
			$this->cart->update($data);
		}

		// This will show cancel data in cart.
		$this->session->set_flashdata('message', array('type'=>'info','message'=>'product has been delete from your cart'));
		redirect('shopping/cart');
	}

	public function update_cart()
	{
		// Recieve post values,calcute them and update
		$cart_info = $_POST['cart'] ;

		// update every cart item
		foreach( $cart_info as $id => $cart)
		{
			$rowid = $cart['rowid'];
			$price = $cart['price'];
			$amount = $price * $cart['qty'];
			$qty = $cart['qty'];

			$data = array(
				'rowid' => $rowid,
				'price' => $price,
				'amount' => $amount,
				'qty' => $qty
			);

			$this->cart->update($data);
		}
		$this->session->set_flashdata('message', array('type'=>'success','message'=>'cart has been update'));
		redirect('shopping/cart');
	}


	public function save_order() 
	{
		//load stripe payment gateway
		$this->load->library('stripegateway');

		//get data from checkout process
		$token = $_POST['stripeToken'];
		$grant_total = $this->input->post('grant_total')*100;
		$description = $this->input->post('description');

		//get the user and if doesnt have stripe customer id, then update user data with stripe customer id
		$user = $this->billing_model->get_user($this->tank_auth->get_user_id());
		if (strlen($user->stripe_customer_id)==0)                            //if no customer id available, then make it
		{
			$stripe_customer_id = $this->stripegateway->create_stripe_customer($user->email, $token)->id;    //make customer id from stripe
			$this->billing_model->update_user_stripe_customer_id($user->id, $stripe_customer_id);    //update user profile field customer id
		} else {
			$stripe_customer_id = $user->stripe_customer_id;    //get available customer id
		}

		//get the data order
		$order = array(
			'date' => date('Y-m-d'),
			'user_id' => $this->tank_auth->get_user_id(),
			'fullname' => $this->input->post('fullname'),
			'address' => $this->input->post('address'),
			'created' => date('Y-m-d H:i:s'),
		);

		//create a new order
		$ord_id = $this->billing_model->insert_order($order);

		//input every product cart to item order
		if ($cart = $this->cart->contents())
		{
			foreach ($cart as $item) {
				$order_detail = array(
							'orderid' => $ord_id,
							'productid' => $item['id'],
							'quantity' => $item['qty'],
							'price' => $item['price']
				);

				// Insert product information with order detail, store in cart also store in database.
				$cust_id = $this->billing_model->insert_order_detail($order_detail);

				//update product stock
				$this->billing_model->update_product_stock($item['id'], $item['qty']);
			}
		}

		//very important process : Charge the order, get the message and get charge id
		$message_result = $this->stripegateway->charge( array(
								'amount' => $grant_total,
								'description' => $description,
								'order_id' => $ord_id,
								'customer_id' => $stripe_customer_id
							));

		//update the order
		$data_update_order = array('message'=>$message_result);
		if($message_result=='succeeded') {
			$data_update_order['stripe_charge_id'] =  $this->stripegateway->get_charge_id();   // get charge_id from stripe
			$this->billing_model->update_order( $ord_id, $data_update_order);                  // update table order 
			$this->cart->destroy();  														   // make sure to destroy all cart items
		}
		$this->session->set_flashdata('message', array('type'=>'info','message'=>'Thanks, your order has ben pay!'));
		return $ord_id;
	}

}