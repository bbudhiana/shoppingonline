<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->config->load('asset');
		$this->load->helper('url');
		$this->load->helper('asset');
	}

	public function index()
	{
		$data['title'] = 'About';
		$this->load->view('templates/header', $data);
		$this->load->view('pages/about', $data);
		$this->load->view('templates/footer');
	}
}