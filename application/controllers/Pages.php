<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Pages Extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->config->load('asset');
		$this->load->helper('url');
		$this->load->helper('asset');
	}
	
    public function view($page='home')
    {
        if (! file_exists(APPPATH.'views/pages/'.$page.'.php')) 
		{
		   show_404();
		}else {
		   $data['title'] = ucfirst($page);

		   $this->load->view('templates/header', $data);
		   $this->load->view('pages/'.$page, $data);
		   $this->load->view('templates/footer', $data);
		}	

    }
}
