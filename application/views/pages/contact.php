<div id="products_e">
	<div class="row">
	    <div class="col-md-6">
	        <div class="well well-sm">
	            <?php echo form_open(base_url().'contact/me', array('class'=>'form-horizontal')); ?>
	                <fieldset>
	                    <legend class="text-center header">Contact Me</legend>
	                    <div class="form-group">
	                        <div class="col-md-10 col-md-offset-1">
	                            <input id="fname" name="name" type="text" placeholder="First Name" class="form-control">
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <div class="col-md-10 col-md-offset-1">
	                            <input id="lname" name="name" type="text" placeholder="Last Name" class="form-control">
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <div class="col-md-10 col-md-offset-1">
	                            <input id="email" name="email" type="text" placeholder="Email Address" class="form-control">
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <div class="col-md-10 col-md-offset-1">
	                            <input id="phone" name="phone" type="text" placeholder="Phone" class="form-control">
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <div class="col-md-10 col-md-offset-1">
	                            <textarea class="form-control" id="message" name="message" placeholder="Enter your massage for us here. We will get back to you within 2 business days." rows="7"></textarea>
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <div class="col-md-12 text-center">
	                            <button type="submit" class="btn btn-primary btn-lg">Submit</button>
	                        </div>
	                    </div>
	                </fieldset>
	            <?php echo form_close(); ?>
	        </div>
	    </div>
	    <div class="col-md-6">
	        <div>
	            <div class="panel panel-default">
	                <div class="text-center header">My Office</div>
	                <div class="panel-body text-center">
	                    <h4>Address</h4>
	                    <div>
	                    Jalan Kakatua 1 Nomor 140<br />
	                    Kota Tangerang, Perumnas 1 Karawaci<br />
	                    #(021) 5511451<br />
	                    #(+62) 8121875050<br />
	                    budhiana.bana@gmail.com<br />
	                    </div>
	                    <hr />
	                    <div id="map1" class="map">
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

<script type="text/javascript">
    jQuery(function ($) {
        function initMap() {
            var myLocation = new google.maps.LatLng(-6.213035, 106.614668);
            var mapOptions = {
                center: myLocation,
                zoom: 16
            };
            var marker = new google.maps.Marker({
                position: myLocation,
                title: "Property Location"
            });
            var map = new google.maps.Map(document.getElementById("map1"),
                mapOptions);
            marker.setMap(map);
        }
        initMap();
    });
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDP3xa4uh1QWhLS9mNc42j_SczamRiOUoE&callback=initMap" type="text/javascript"></script>
<style>
    .map {
        min-width: 300px;
        min-height: 300px;
        width: 100%;
        height: 100%;
    }

    .header {
        background-color: #F5F5F5;
        color: #36A0FF;
        height: 70px;
        font-size: 27px;
        padding: 10px;
    }
</style>