
<div id="products_e">
	<!-- Introduction Row -->
	<h1 class="my-4">About Me
	  <small>It's Nice to Meet You!</small>
	</h1>
	<p>The 'about' section usually tells about a personal or company that is behind a website. The information contained in 'about' is anyone involved in the development of the website and what role they are doing to make this website available. Thus, it follows about those who work so hard that this website exists.</p>

	<!-- Team Members Row -->
	<div class="row">
	  <div class="col-lg-12">
	    <h2 class="my-4">Our Team</h2>
	  </div>
	  <div class="col-lg-4 col-sm-6 text-center mb-4">
	    <img class="rounded-circle img-fluid d-block mx-auto" src="<?php echo asset_url() . 'img/bana_one.jpg' ?>" alt="bana as Programmer">
	    <h3>Bana Budhiana
	      <small>Programmer</small>
	    </h3>
	    <p>This person is programming for a long time. His experience leads to wisdom and purity in the world of programming. At this point he is looking for a place to channel his natural talent for an outstanding achievement of opportunity.
	    </p>
	  </div>
	  <div class="col-lg-4 col-sm-6 text-center mb-4">
	    <img class="rounded-circle img-fluid d-block mx-auto" src="<?php echo asset_url() . 'img/bana_two.jpg' ?>" alt="bana as Analyst">
	    <h3>Budhiana Bana
	      <small>Analyst</small>
	    </h3>
	    <p>Basically, the programming job also requires the ability of the analyst, therefore this person also has the ability to perform system analysis and planning in project development.</p>
	  </div>
	  <div class="col-lg-4 col-sm-6 text-center mb-4">
	    <img class="rounded-circle img-fluid d-block mx-auto" src="<?php echo asset_url() . 'img/bana_three.jpg' ?>" alt="bana as database advisor">
	    <h3>B Budhiana
	      <small>Database Advisor</small>
	    </h3>
	    <p>In addition, the ability to process the database is also no doubt because the database is a technology that can not be separated from the world of programming and data analysis.</p>
	  </div>
	</div>
</div>