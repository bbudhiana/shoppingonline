  <div id="products_e" align="center">

      <h2 id="head" align="center">Products</h2>
      <div class="row">
      <?php
      
        // "$products" send from "shopping" controller,its stores all product which available in database. 
        foreach ($products as $product) {
          $id = $product['serial'];
          $name = $product['name'];
          $description = $product['description'];
          $price = $product['price'];
        ?>
         
          <div id='product_div'>  
                <div id='image_div'>
                  <img src="<?php echo upload_url() . $product['picture'].'_thumb'.$product['picture_ext'] ?>"/>
                </div>
                <div id='info_product'>
                    <div id='name'><?php echo $name; ?></div>
                    <div id='desc'><?php echo $description; ?></div>
                    <div id='rs'><b>Price</b>:<big style="color:green">
                        $<?php echo $price; ?></big>
                    </div>
                    <?php
                    
                        // Create form and send values in 'shopping/add' function.
                        echo form_open('shopping/add');
                        echo form_hidden('id', $id);
                        echo form_hidden('name', $name);
                        echo form_hidden('price', $price);
                    ?> 
                    <div id="quantity">
                      <b>Quantity</b>: 
                          <?php
                            $options= array();
                            for($i=1;$i<=$product['stock'];$i++) {
                              $options[$i]=$i;
                            }
                            if($product['stock']==0) $options[0]=0;
                            echo form_dropdown('qty', $options, '1');
                          ?>
                    </div>
                    
                </div> 
                <div id='add_button'>
                    <?php
                    $btn = array(
                        'class' => 'btn btn-primary',
                        'value' => 'Add to Cart',
                        'name' => 'action'
                    );

                    if($product['stock']==0) $btn['disabled'] = 'disabled';
                    
                    // Submit Button.
                    echo form_submit($btn);
                    echo form_close();
                    ?>
                </div>
          </div>

        <?php } ?>
        </div>

        <div class="row">
          <?php echo $this->pagination->create_links(); ?>
        </div>

  </div>