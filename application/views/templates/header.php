<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Web Application <?php echo $title; ?></title>

    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo asset_url() . 'bower_components/bootstrap/dist/css/bootstrap.css'; ?>">
    <link rel="stylesheet" href="<?php echo asset_url() . 'bower_components/toastr/toastr.css'; ?>">
    <link rel="stylesheet" href="<?php echo asset_url() . 'bower_components/datatables/media/css/jquery.datatables.css'; ?>">
    <link rel="stylesheet" href="<?php echo asset_url() . 'css/ie10-viewport-bug-workaround.css'; ?>">
    <link rel="stylesheet" href="<?php echo asset_url() . 'css/style.css'; ?>">

     <!-- Scripts -->
    <script src="<?php echo asset_url() . 'bower_components/jquery/dist/jquery.js'; ?>"></script>
    <script src="<?php echo asset_url() . 'bower_components/bootstrap/dist/js/bootstrap.js' ?>"></script>
    <script src="<?php echo asset_url() . 'bower_components/toastr/toastr.js' ?>"></script>
    <script src="<?php echo asset_url() . 'bower_components/datatables/media/js/jquery.dataTables.js' ?>"></script>
    <script src="<?php echo asset_url() . 'js/ie10-viewport-bug-workaround.js' ?>"></script>
    
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo asset_url() . 'js/ie-emulation-modes-warning.js'; ?>"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container">

	    <!-- Static navbar -->
	    <nav class="navbar navbar-default">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <span class="navbar-brand">Shopping Online</span>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav">
	            <li class="<?php if(uri_string()=='') echo 'active'; ?>"><?php echo anchor('/', 'Home'); ?></li>
	            <li class="<?php if(uri_string()=='about') echo 'active'; ?>"><?php echo anchor('about', 'About'); ?></li>
	            <li class="<?php if(uri_string()=='contact') echo 'active'; ?>"><?php echo anchor('contact', 'Contact'); ?></li>
	            <?php if($this->tank_auth->is_logged_in()): ?>
	            	<?php if($this->tank_auth->is_admin()): ?>
			            <li class="dropdown">
			              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin Content <span class="caret"></span></a>
			              <ul class="dropdown-menu">
			                <li><?php echo anchor('admin', 'User'); ?></li>
			                <li><?php echo anchor('admin/products', 'Products'); ?></li>
			                <li><?php echo anchor('admin/orders', 'Orders'); ?></li>
			                <li role="separator" class="divider"></li>
			                
			              </ul>
			            </li>
	            	<?php endif; ?>
	            <?php endif; ?>
	          </ul>
	          <ul class="nav navbar-nav navbar-right">
	            <li><a href="<?php echo base_url().'shopping/cart' ?>">My Cart 
	            		<?php if($this->cart->total_items()>0) echo '<span class="badge progress-bar-danger">'.$this->cart->total_items().'</span>'; ?>
	            	</a>
	            </li>
	            <?php if($this->tank_auth->is_logged_in()): ?>
		            <li class="dropdown">
		              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">User Content <span class="caret"></span></a>
		              <ul class="dropdown-menu">
		                <li><?php echo anchor('user', 'Account'); ?></li>
		                <li><?php echo anchor('user/profile', 'Profile'); ?></li>
		                <li><?php echo anchor('user/orders', 'Orders'); ?></li>
		                <li role="separator" class="divider"></li>
		                <li><?php echo anchor('auth/logout','Logout'); ?></li>
		              </ul>
		            </li>
		        <?php else: ?>
		        	<li>
		        		<?php echo anchor('auth/login','Login'); ?>
		        	</li>
	        	<?php endif; ?>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div><!--/.container-fluid -->
	    </nav>
