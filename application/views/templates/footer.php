      <hr>
      <footer>
        <p>&copy; 2017 Company, Inc.</p>
      </footer>

    </div> <!-- /container -->

   
    <?php if($this->session->flashdata('message')): ?>
	    <script>
        <?php $msg = $this->session->flashdata('message'); ?>
	    	toastr.<?php echo $msg['type']; ?>('<?php echo $msg['message']; ?>');
	    </script>
	<?php endif; ?>
</body>
</html>