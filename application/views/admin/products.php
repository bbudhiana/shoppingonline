  <div id="products_e" align="center">

    <h2 id="head" align="center">List Products</h2>
    <div class="row">
      <div class="col-md-6">
        &nbsp;
      </div>
      <div class="col-md-4">
          
      </div>
      <div class="col-md-2">
          <?php echo anchor('admin/edit_product', 'Add Product', array('class'=>'btn btn-primary')); ?>
      </div>
      
    </div>
    <div class="row">
      <table id="products" class="display" cellspacing="0" width="90%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Picture</th>
                <th>Stock</th>
                <th>Control</th>
            </tr>
        </thead>

        <tbody>
            <?php
              $i = 1;
              foreach ($products as $product) {
              ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $product['name']; ?></td>
                <td style="width: 30%"><?php echo $product['description']; ?></td>
                <td>$<?php echo $product['price']; ?></td>
                <td style="width: 20%"></td>
                <td><?php echo $product['stock']; ?></td>
                <td>
                  <a  value="Delete Product" onclick="delete_product('<?php echo $product['serial']?>')"><span class="glyphicon glyphicon-trash"></span></a>
                  <?php echo anchor('admin/edit_product/'.$product['serial'], '<span class="glyphicon glyphicon-edit"></span>'); ?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="row">
      <?php echo $this->pagination->create_links(); ?>
    </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
      $('#products').DataTable(
        {
          "ordering": false,
          "searching": false,
          "paging": false,
          "info": false
      });

  } );

  // To conform clear all data in cart.
  function delete_product(id) {
    var result = confirm('Are you sure want to delete product?');

    if (result) {
      window.location = "<?php echo base_url(); ?>admin/remove_product/"+id;
    } else {
      return false; // cancel button
    }
  }

</script>