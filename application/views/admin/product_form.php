<?php

$name = array(
	'name'	=> 'name',
	'id'	=> 'name',
	'class'=>'form-control',
	'placeholder' => 'Product Name',
);

$description = array (
    'name' => 'description',
    'cols' => 6,
    'rows' => 3,
    'placeholder' => 'Description',
    'class' => 'form-control',
);

$price = array(
	'name'	=> 'price',
	'id'	=> 'price',
	'class'=>'form-control',
	'placeholder' => 'Product Price',
	'size' => 6,
);

$stock = array(
	'name'	=> 'stock',
	'id'	=> 'stock',
	'class'=>'form-control',
	'placeholder' => 'Product Stock',
);

$file = array(
	'name'	=> 'userFile',
	'id'	=> 'userFile',
	'type' => 'file',
	'placeholder' => 'Product Stock',
);


?>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Product</div>
            <div class="panel-body">
            	<?php echo form_open_multipart($this->uri->uri_string(), array('class'=>'form-horizontal')); ?>
            		<?php if($product) echo form_hidden('productid', $product->serial); ?>
					<div class="form-group<?php echo (form_error($name['name']))? ' has-error' : ''; ?>">
	               	    <label for="name" class="col-md-4 control-label">Name</label>

	               	    <div class="col-md-6">
	               	        <?php echo form_input($name, $product?$product->name:''); ?>
								
								<?php if (form_error($name['name'])): ?>
	               	            <span class="help-block">
	               	                <strong><?php echo form_error($name['name']); ?>
	               	                </strong>
	               	            </span>
	               	        <?php endif; ?>
	               	    </div>
	               	</div>

					<div class="form-group<?php echo (form_error($description['name']))? ' has-error' : ''; ?>">
	               	    <label for="description" class="col-md-4 control-label">Description</label>

	               	    <div class="col-md-6">
	               	        <?php echo form_textarea($description, $product?$product->description:''); ?>
								
								<?php if (form_error($description['name'])): ?>
	               	            <span class="help-block">
	               	                <strong><?php echo form_error($description['name']); ?>
	               	                </strong>
	               	            </span>
	               	        <?php endif; ?>
	               	    </div>
	               	</div>

					<div class="form-group<?php echo (form_error($price['name']))? ' has-error' : ''; ?>">
	               	    <label for="price" class="col-md-4 control-label">Price</label>

	               	    <div class="col-xs-3">
	               	        <?php echo form_input($price, $product?$product->price:''); ?>
								
								<?php if (form_error($price['name'])): ?>
	               	            <span class="help-block">
	               	                <strong><?php echo form_error($price['name']); ?>
	               	                </strong>
	               	            </span>
	               	        <?php endif; ?>
	               	    </div>
	               	</div>

	               	<div class="form-group<?php echo (form_error($stock['name']))? ' has-error' : ''; ?>">
	               	    <label for="stock" class="col-md-4 control-label">Stock</label>

	               	    <div class="col-xs-3">
	               	        <?php echo form_input($stock, $product?$product->stock:''); ?>
								
								<?php if (form_error($stock['name'])): ?>
	               	            <span class="help-block">
	               	                <strong><?php echo form_error($stock['name']); ?>
	               	                </strong>
	               	            </span>
	               	        	<?php endif; ?>
	               	    </div>
	               	</div>

	               	<div class="form-group<?php echo (form_error($file['name']))? ' has-error' : ''; ?>">
	               	    <label for="file" class="col-md-4 control-label">Image</label>

	               	    <div class="col-md-6" style="margin-top: 7px">
	               	        <?php echo form_input($file); ?>
								
								<?php if (form_error($file['name'])): ?>
	               	            <span class="help-block">
	               	                <strong><?php echo form_error($file['name']); ?>
	               	                </strong>
	               	            </span>
	               	        	<?php endif; ?>
	               	    </div>
	               	</div>
	               	
					<hr />
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button name="submit" type="submit" value="submit" class="btn btn-primary">
                                Save Product
                            </button>
                            <?php echo anchor('admin/products', 'Back to Product List'); ?>
                        </div>
                    </div>
				<?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>