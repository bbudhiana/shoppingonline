  <div id="products_e" align="center">

    <h2 id="head" align="center">List Users</h2>
    <div class="row">
      <table id="users" class="display" cellspacing="0" width="90%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Username</th>
                <th>Email</th>
                <th>Role</th>
                <th>Activated</th>
                <th>Fullname</th>
                <th>Last Login</th>
                <th>Control</th>
            </tr>
        </thead>

        <tbody>
            <?php
              $i = 1;
              foreach ($users as $user) {
                $username = $user['username'];
                $email = $user['email'];
                $role =$user['role'];
                $activated = $user['activated']?'Active':'Not Active';

                $user_profile = $this->billing_model->get_user_profile($user['id']);

                $fullname = is_null($user_profile)?'':$user_profile->fullname;
                $last_login = $user['last_login'];
              ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $username; ?></td>
                <td><?php echo $email; ?></td>
                <td><?php echo $role; ?></td>
                <td><?php echo $activated; ?></td>
                <td><?php echo $fullname; ?></td>
                <td><?php echo $last_login; ?></td>
                <td>
                  <a  value="Delete User" onclick="delete_user('<?php echo $user['id']?>')"><span class="glyphicon glyphicon-trash"></span></a>
                  <?php echo anchor('admin/edit_user/'.$user['id'], '<span class="glyphicon glyphicon-edit"></span>'); ?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="row">
      <?php echo $this->pagination->create_links(); ?>
    </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
      $('#users').DataTable(
        {
          "ordering": false,
          "searching": false,
          "paging": false,
          "info": false
      });

  } );

  // To conform clear all data in cart.
  function delete_user(id) {
    var result = confirm('Are you sure want to delete user?');

    if (result) {
      window.location = "<?php echo base_url(); ?>admin/remove_user/"+id;
    } else {
      return false; // cancel button
    }
  }

</script>