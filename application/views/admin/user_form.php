<?php

$activation = array(
	'name'	=> 'activated',
	'id'	=> 'activated',
	'class'=>'form-control',
	'placeholder' => 'Activation',
);

$role = array(
	'name'	=> 'role',
	'id'	=> 'role',
	'class'=>'form-control',
	'placeholder' => 'Activation',
);

 $user_profile = $this->billing_model->get_user_profile($user->id);

 $fullname = is_null($user_profile)?'':$user_profile->fullname;
?>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Edit User</div>
            <div class="panel-body">
            	<?php echo form_open($this->uri->uri_string(), array('class'=>'form-horizontal')); ?>
					<div class="form-group">
	               	    <label for="username" class="col-md-4 control-label">Username</label>

	               	    <div class="col-md-6" style="margin-top: 7px;">
	               	        <?php echo $user->username; ?>
	               	    </div>
	               	</div>

					<div class="form-group">
	               	    <label for="email" class="col-md-4 control-label">Email</label>

	               	    <div class="col-md-6" style="margin-top: 7px;">
	               	        <?php echo $user->email; ?>
	               	    </div>
	               	</div>

	               	<div class="form-group">
	               	    <label for="email" class="col-md-4 control-label">Fullname</label>

	               	    <div class="col-md-6" style="margin-top: 7px;">
	               	        <?php echo $fullname; ?>
	               	    </div>
	               	</div>

	               	<div class="form-group">
	               	    <label for="activation" class="col-md-4 control-label">Activation</label>

	               	    <div class="col-md-6">
	               	        <?php echo form_dropdown($activation, array('Not active', 'Active'),$user->activated); ?>
	               	    </div>
	               	</div>
	               	<div class="form-group">
	               	    <label for="role" class="col-md-4 control-label">Role</label>

	               	    <div class="col-md-6">
	               	        <?php echo form_dropdown($role, array('Admin'=>'Admin', 'User'=>'User'), $user->role); ?>
	               	    </div>
	               	</div>
					<hr />
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button name="submit" type="submit" value="submit" class="btn btn-primary">
                                Edit User
                            </button>
                            <?php echo anchor('admin', 'Back to User List'); ?>
                        </div>
                    </div>
				<?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>