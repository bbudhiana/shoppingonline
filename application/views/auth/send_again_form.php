<?php
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
	'class'=>'form-control',
	'placeholder' =>'email',
);
?>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Send Again Activation Email</div>
            <div class="panel-body">
		
			<?php echo form_open($this->uri->uri_string(), array('class'=>'form-horizontal')); ?>
				<div class="form-group<?php echo (form_error($email['name']) || isset($errors[$email['name']]))? ' has-error' : ''; ?>">
                    <label for="login" class="col-md-4 control-label">Input your email</label>

                    <div class="col-md-6">
                        <?php echo form_input($email); ?>
						
						<?php if (form_error($email['name']) || isset($errors[$email['name']])): ?>
                            <span class="help-block">
                                <strong><?php echo form_error($email['name']); ?>
                                	    <?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?>
                                </strong>
                            </span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group">
               	    <div class="col-md-8 col-md-offset-4">
               	        <?php echo form_submit('send', 'Get Activation', array('class'=>'btn btn-primary')); ?>
               	    </div>
               	</div>
			<?php echo form_close(); ?>
			

			</div>
	    </div>
	</div>
</div>