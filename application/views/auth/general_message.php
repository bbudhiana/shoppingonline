<div class="row">
    <div class="col-md-8 col-md-offset-2">
    	<div class="panel panel-default">
            <div class="panel-heading">Process Successfully</div>
            <div class="panel-body">
				<?php echo $message; ?> - <?php echo anchor('/auth/login', 'Back Login', array('class'=>'btn btn-link')); ?>
			</div>
        </div>
    </div>
</div>