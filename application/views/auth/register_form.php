<?php
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
		'size'	=> 30,
		'class'=>'form-control',
		'placeholder' => 'Username',
	);
}
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'type' => 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
	'class'=>'form-control',
	'placeholder' => 'Email',
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'type' =>'password',
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
	'class'=>'form-control',
	'placeholder' => 'Password',
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'type' =>'password',
	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
	'class'=>'form-control',
	'placeholder' => 'Confirm Password',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
	'class'=>'form-control',
	'placeholder' => 'Captcha',
);
?>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Register</div>
            <div class="panel-body">
            	<?php echo form_open($this->uri->uri_string(), array('class'=>'form-horizontal')); ?>
	            	<?php if ($use_username): ?>
					<div class="form-group<?php echo (form_error($username['name']) || isset($errors[$username['name']]))? ' has-error' : ''; ?>">
	               	    <label for="username" class="col-md-4 control-label">Username</label>

	               	    <div class="col-md-6">
	               	        <?php echo form_input($username); ?>
								
								<?php if (form_error($username['name']) || isset($errors[$username['name']])): ?>
	               	            <span class="help-block">
	               	                <strong><?php echo form_error($username['name']); ?>
	               	                	    <?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?>
	               	                </strong>
	               	            </span>
	               	        	<?php endif; ?>
	               	    </div>
	               	</div>
	            	<?php endif; ?>

					<div class="form-group<?php echo (form_error($email['name']) || isset($errors[$email['name']]))? ' has-error' : ''; ?>">
	               	    <label for="email" class="col-md-4 control-label">Email</label>

	               	    <div class="col-md-6">
	               	        <?php echo form_input($email); ?>
								
								<?php if (form_error($email['name']) || isset($errors[$email['name']])): ?>
	               	            <span class="help-block">
	               	                <strong><?php echo form_error($email['name']); ?>
	               	                	    <?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?>
	               	                </strong>
	               	            </span>
	               	        	<?php endif; ?>
	               	    </div>
	               	</div>
                	
                	<div class="form-group<?php echo (form_error($password['name']))? ' has-error' : ''; ?>">
	               	    <label for="password" class="col-md-4 control-label">Password</label>

	               	    <div class="col-md-6">
	               	        <?php echo form_input($password); ?>
								
								<?php if (form_error($password['name'])): ?>
	               	            <span class="help-block">
	               	                <strong><?php echo form_error($password['name']); ?></strong>
	               	            </span>
	               	        	<?php endif; ?>
	               	    </div>
	               	</div>

	               	<div class="form-group<?php echo (form_error($confirm_password['name']))? ' has-error' : ''; ?>">
	               	    <label for="confirm_password" class="col-md-4 control-label">Confirm Password</label>

	               	    <div class="col-md-6">
	               	        <?php echo form_input($confirm_password); ?>
								
								<?php if (form_error($confirm_password['name'])): ?>
	               	            <span class="help-block">
	               	                <strong><?php echo form_error($confirm_password['name']); ?></strong>
	               	            </span>
	               	        	<?php endif; ?>
	               	    </div>
	               	</div>

	               	<?php if ($captcha_registration): ?>
					 <div class="form-group<?php echo (form_error($captcha['name']) || isset($errors[$captcha['name']]))? ' has-error' : ''; ?>">
                        <label for="captcha" class="col-md-4 control-label">Verify Code</label>

                        <div class="col-md-6">
                            <?php echo $captcha_html; ?> <br />
                            <?php echo form_input($captcha); ?>
							
							<?php if (form_error($captcha['name']) || isset($errors[$captcha['name']])): ?>
                                <span class="help-block">
                                    <strong><?php echo form_error($captcha['name']); ?>
                                    	    <?php echo isset($errors[$captcha['name']])?$errors[$captcha['name']]:''; ?>
                                    </strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>

	                <?php endif; ?>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Register
                            </button>
                            <?php echo anchor('/auth/login', 'Back Login', array('class'=>'btn btn-link')); ?>
                        </div>
                    </div>
				<?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>