<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
	'class'=>'form-control',
	'placeholder' => 'Email',
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'type' =>'password',
	'size'	=> 30,
	'class'=>'form-control',
	'placeholder' => 'Password',
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	//'style' => 'margin:0;padding:0',
	'type'=> 'checkbox',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
	'class'=>'form-control',
);
?>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Login</div>
            <div class="panel-body">
                <?php echo form_open($this->uri->uri_string(), array('class'=>'form-horizontal')); ?>

                    <div class="form-group<?php echo (form_error($login['name']) || isset($errors[$login['name']]))? ' has-error' : ''; ?>">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <?php echo form_input($login); ?>
							
							<?php if (form_error($login['name']) || isset($errors[$login['name']])): ?>
                                <span class="help-block">
                                    <strong><?php echo form_error($login['name']); ?>
                                    	    <?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
                                    </strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group<?php echo (form_error($password['name']) || isset($errors[$password['name']]))? ' has-error' : ''; ?>">
                        <label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <?php echo form_input($password); ?>
							
							<?php if (form_error($password['name']) || isset($errors[$password['name']])): ?>
                                <span class="help-block">
                                    <strong><?php echo form_error($password['name']); ?>
                                    	    <?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
                                    </strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
					
					<?php if ($show_captcha): ?>
                    <div class="form-group<?php echo (form_error($captcha['name']) || isset($errors[$captcha['name']]))? ' has-error' : ''; ?>">
                        <label for="captcha" class="col-md-4 control-label">Verify Code</label>

                        <div class="col-md-6">
                            <?php echo $captcha_html; ?> <br />
                            <?php echo form_input($captcha); ?>
							
							<?php if (form_error($captcha['name']) || isset($errors[$captcha['name']])): ?>
                                <span class="help-block">
                                    <strong><?php echo form_error($captcha['name']); ?>
                                    	    <?php echo isset($errors[$captcha['name']])?$errors[$captcha['name']]:''; ?>
                                    </strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                   <?php endif; ?>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <?php echo form_checkbox($remember); ?> Remember Me
                                    
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Login
                            </button>

                            <?php echo anchor('/auth/forgot_password', 'Forgot Your Password?', array('class'=>'btn btn-link')); ?>
                            <?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'Register'); ?>
                        </div>
                    </div>
				<?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>