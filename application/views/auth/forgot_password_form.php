<?php

if ($this->config->item('use_username', 'tank_auth')) {
	$login_label = 'Email or login';
} else {
	$login_label = 'Email';
}
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
	'class'=>'form-control',
	'placeholder' => $login_label,
);
?>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Forgot Password</div>
            <div class="panel-body">
				
				<?php echo form_open($this->uri->uri_string(), array('class'=>'form-horizontal')); ?>
					<div class="form-group<?php echo (form_error($login['name']) || isset($errors[$login['name']]))? ' has-error' : ''; ?>">
                        <label for="login" class="col-md-4 control-label"><?php echo $login_label; ?></label>

                        <div class="col-md-6">
                            <?php echo form_input($login); ?>
							
							<?php if (form_error($login['name']) || isset($errors[$login['name']])): ?>
                                <span class="help-block">
                                    <strong><?php echo form_error($login['name']); ?>
                                    	    <?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
                                    </strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <?php echo form_submit('reset', 'Get a new password', array('class'=>'btn btn-primary')); ?>

                            <?php echo anchor('/auth/login', 'Back to Login', array('class'=>'btn btn-link')); ?>
                        </div>
                    </div>
				<?php echo form_close(); ?>

            </div>
        </div>
    </div>
</div>