<div id="products_e" align="center">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
    			<h2>Invoice</h2>
    		</div>
    		<hr>
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-6 text-left">
    		<strong>Order</strong> #<?php echo $order->serial; ?>
    	</div>
    	<div class="col-md-6 text-right">
    		<strong>Shippment Address</strong> <br />
    		<?php echo $order->fullname; ?> - <?php echo $order->address; ?>
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Order summary</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Item</strong></td>
        							<td class="text-center"><strong>Price</strong></td>
        							<td class="text-center"><strong>Quantity</strong></td>
        							<td class="text-right"><strong>Totals</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
    							<?php $grant_total = 0 ;?>
    							<?php foreach($order_detail as $item): ?>
    							<?php
                                   $product = $this->db->where('serial', $item->productid)->get('products')->row();
                                   $grant_total = $grant_total + ($item->price * $item->quantity); 
    							?>
    							<tr>
    								<td><?php echo $product->name; ?></td>
    								<td class="text-center">$<?php echo $item->price ;?></td>
    								<td class="text-center"><?php echo $item->quantity ;?></td>
    								<td class="text-right">$<?php echo $item->price * $item->quantity; ?></td>
    							</tr>
    							<?php endforeach; ?>
                                
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right"><?php echo $grant_total; ?></td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
