<?php

$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'type' =>'password',
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
	'class'=>'form-control',
	'placeholder' => 'Password',
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'type' =>'password',
	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
	'class'=>'form-control',
	'placeholder' => 'Confirm Password',
);
?>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">User Account</div>
            <div class="panel-body">
            	<?php echo form_open($this->uri->uri_string(), array('class'=>'form-horizontal')); ?>
					<div class="form-group">
	               	    <label for="username" class="col-md-4 control-label">Username</label>
	               	    <div class="col-md-6" style="margin-top:7px;">
	               	        <?php echo $user->username; ?>
	               	    </div>
	               	</div>

					<div class="form-group">
	               	    <label for="email" class="col-md-4 control-label">Email</label>

	               	    <div class="col-md-6" style="margin-top:7px;">
	               	        <?php echo $user->email; ?>
	               	    </div>
	               	</div>
                	
                	<div class="form-group<?php echo (form_error($password['name']))? ' has-error' : ''; ?>">
	               	    <label for="password" class="col-md-4 control-label">Password</label>

	               	    <div class="col-md-6">
	               	        <?php echo form_input($password); ?>
								
								<?php if (form_error($password['name'])): ?>
	               	            <span class="help-block">
	               	                <strong><?php echo form_error($password['name']); ?></strong>
	               	            </span>
	               	        	<?php endif; ?>
	               	    </div>
	               	</div>

	               	<div class="form-group<?php echo (form_error($confirm_password['name']))? ' has-error' : ''; ?>">
	               	    <label for="confirm_password" class="col-md-4 control-label">Confirm Password</label>

	               	    <div class="col-md-6">
	               	        <?php echo form_input($confirm_password); ?>
								
								<?php if (form_error($confirm_password['name'])): ?>
	               	            <span class="help-block">
	               	                <strong><?php echo form_error($confirm_password['name']); ?></strong>
	               	            </span>
	               	        	<?php endif; ?>
	               	    </div>
	               	</div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Change Account
                            </button>
                        </div>
                    </div>
				<?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>