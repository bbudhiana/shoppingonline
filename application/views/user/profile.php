<?php

$address = array (
            'name' => 'address',
            'cols' => 6,
            'rows' => 3,
            'placeholder' => 'Shipping Address',
            'class' => 'form-control',
);

$fullname = array (
    'name' => 'fullname',
    'id'   => 'fullname',
    'placeholder' => 'Fullname',
    'class' => 'form-control',
);
?>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">User Profile</div>
            <div class="panel-body">
            	<?php echo form_open($this->uri->uri_string(), array('class'=>'form-horizontal')); ?>
					<div class="form-group">
	               	    <label for="username" class="col-md-4 control-label">Username</label>
	               	    <div class="col-md-6" style="margin-top:7px;">
	               	        <?php echo $user->username; ?>
	               	    </div>
	               	</div>

					<div class="form-group">
	               	    <label for="email" class="col-md-4 control-label">Email</label>

	               	    <div class="col-md-6" style="margin-top:7px;">
	               	        <?php echo $user->email; ?>
	               	    </div>
	               	</div>
                	
                	<div class="form-group">
	               	    <label for="fullname" class="col-md-4 control-label">Fullname</label>

	               	    <div class="col-md-6">
	               	        <?php echo form_input($fullname, $user_profile->fullname); ?>
	               	    </div>
	               	</div>

	               	<div class="form-group">
	               	    <label for="address" class="col-md-4 control-label">Address</label>

	               	    <div class="col-md-6">
	               	        <?php echo form_textarea($address, $user_profile->address); ?>
	               	    </div>
	               	</div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button name="submit" type="submit" value="submit" class="btn btn-primary">
                                Change Profile
                            </button>
                        </div>
                    </div>
				<?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>