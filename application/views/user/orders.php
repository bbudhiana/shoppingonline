  <div id="products_e" align="center">

    <h2 id="head" align="center">Your Orders</h2>
    <div class="row">
      <table id="orders" class="display" cellspacing="0" width="90%">
        <thead>
            <tr>
                <th>Order ID</th>
                <th>To</th>
                <th>Shipping Address</th>
                <th>Order Date</th>
                <th>Total</th>
                <th>Stripe Pay</th>
                <th>Stripe Status</th>
            </tr>
        </thead>

        <tbody>
            <?php
              $i = 1;
              foreach ($orders as $order) {
                $fullname = $order['fullname'];
                $address = $order['address'];
                $created = $order['created'];
                $total = $this->billing_model->get_total_item_order($order['serial'])->total;
                $charge_info = $this->stripegateway->get_charge_info($order['stripe_charge_id']);
              ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $fullname; ?></td>
                <td><?php echo $address; ?></td>
                <td><?php echo $created; ?></td>
                <td>$<?php echo $total; ?></td>
                <td>$<?php echo $charge_info->amount/100; ?></td>
                <td>
                  <?php 
                      if($charge_info->status=='succeeded') echo '<span class="label label-success">Success</span>'; 
                      if($charge_info->status=='pending') echo '<span class="label label-warning">Pending</span>'; 
                      if($charge_info->status=='failed') echo '<span class="label label-danger">Failed</span>'; 
                  ?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="row">
      <?php echo $this->pagination->create_links(); ?>
    </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
      $('#orders').DataTable(
        {
          "ordering": false,
          "searching": false,
          "paging": false,
          "info": false
      });
  } );
</script>