  <div id="products_e" align="center">
    <h2 id="head" align="center">Checkout</h2>
    <?php
        $grand_total = 0;
        // Calculate grand total and summary buy.
        if ($cart = $this->cart->contents()):
          $summary = '';
          $lastArray = end($cart);
          $lastID = $lastArray['id'];
          foreach ($cart as $item):
            $grand_total = $grand_total + $item['subtotal'];
            $summary = $summary . $item['name'] .'('.$item['qty']. ')';
            if($lastID<>$item['id']) $summary = $summary . ', ';
          endforeach;
        endif;

        //get user info that have stripe customer id and user profile that have address
        $user_info = $this->db->where('id', $this->tank_auth->get_user_id())->get('users')->row();
        $user_profile = $this->db->where('user_id', $user_info->id)->get('user_profiles')->row();

        //if user profile empty then create one for the user
        if(!$user_profile) 
        {
             $this->db->insert('user_profiles', array('user_id'=>$user_info->id));
             $user_profile = $this->db->where('user_id', $user_info->id)->get('user_profiles')->row();
        }

        //create array properties for address input form
        $address = array (
            'name' => 'address',
            'cols' => 6,
            'rows' => 3,
            'placeholder' => 'Shipping Address',
            'class' => 'form-control',
        );

        $fullname = array (
            'name' => 'fullname',
            'id'   => 'fullname',
            'placeholder' => 'Fullname',
            'class' => 'form-control',
        );
    ?>
    <div class="table-responsive">
      <?php $hidden = array('user_id'=> $user_info->id,'grant_total' => $grand_total, 'description'=>$summary);?>
      <?php echo form_open($this->uri->uri_string(), '', $hidden); ?>
        <input type="hidden" name="command" />
        <div align="center">
          <h3 align="center">Summary</h3>
          <table id="table" class="table  table-striped">
            <tr><td>Order Total:</td><td><strong>$<?php echo number_format($grand_total, 2); ?></strong></td></tr>
            <tr><td>Username:</td><td><?php echo $user_info->username; ?>(<?php echo $user_info->email; ?>)</td></tr>
            <tr>
              <td>You order: </td>
              <td><?php echo $summary; ?></td>
            </tr>
            <tr><td>Fullname:</td>
                <td>
                  
                  <div class="form-group<?php echo (form_error($fullname['name']))? ' has-error' : ''; ?>">

                      <div class="col-md-7" style="margin-left: -15px;">
                          <?php echo form_input($fullname, $user_profile->fullname);?>
                            <?php if (form_error($fullname['name'])): ?>
                              <span class="help-block">
                                  <strong><?php echo form_error($fullname['name']); ?></strong>
                              </span>
                            <?php endif; ?>
                      </div>
                  </div>

                </td>
            </tr>
            <tr><td>Shipping Address:</td>
                <td>
                  
                  <div class="form-group<?php echo (form_error($address['name']))? ' has-error' : ''; ?>">

                      <div class="col-md-7" style="margin-left: -15px;">
                          <?php echo form_textarea($address, $user_profile->address);?>
                            <?php if (form_error($address['name'])): ?>
                              <span class="help-block">
                                  <strong><?php echo form_error($address['name']); ?></strong>
                              </span>
                            <?php endif; ?>
                      </div>
                  </div>

                </td>
            </tr>
            <tr>
              <td>  
                &nbsp;
              </td>
              <td>
                  <!-- <input class ='btn btn-primary' type="submit" value="Pay" /> -->
                  <script
                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                    data-key="pk_test_fqvr16DTTcD0pWXRnzFmmN9V"
                    data-amount=<?php echo $grand_total*100; ?>
                    data-name="My Online Site"
                    data-description="<?php echo $summary; ?>"
                    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                    data-locale="auto">
                  </script>
              </td>
            </tr>
          </table>
        </div>
      <?php echo form_close(); ?>

    </div>
</div>