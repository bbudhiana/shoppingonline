<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Billing_model
 *
 * This model represents every model that envolve in shopping online transaction:
 * - product,
 * - order,
 * - order detail,
 * - costumer
 *
 * @package	Shopping_online
 * @author	Bana Budhiana
 */
class Billing_model extends CI_Model {
    
    /**
     * Get all details store in 'products' table in database
     * @return array 
     */
    public function get_all()
	{
		$query = $this->db->get('products');
		return $query->result_array();
	}

	/**
	 * Get order by order id
	 * @param  int $order_id 
	 * @return object   
	 */
	public function get_order($order_id)
	{
		$query = $this->db->where('serial', $order_id)->get('orders');
		return $query->row();

	}

	/**
	 * Get count of all orders
	 * @return int [description]
	 */
	public function get_count_order()
	{
		return $this->db->get('orders')->num_rows();
	}

	/**
	 * Get order detail by order id
	 * @param  int $order_id 
	 * @return object          
	 */
	public function get_order_detail($order_id)
	{
		$query = $this->db->where('orderid', $order_id)->get('order_detail');
		return $query->result();
	}
	
    /**
     * Insert table order with data and return an integer id of new record
     * @param  array $data 
     * @return int   
     */
	public function insert_order($data)
	{
		$this->db->insert('orders', $data);
		$id = $this->db->insert_id();
		return (isset($id)) ? $id : FALSE;
	}

	/**
	 * Update table order base on order id 
	 * @param  int $order_id
	 * @param  array $data     
	 * @return void           
	 */
	public function update_order($order_id, $data)
	{
		$this->db->where('serial', $order_id);
		$this->db->update('orders', $data);
	}
	
	/**
	 * Insert ordered product detail in "order_detail" table in database
	 * @param  array $data 
	 * @return void  
	 */
	public function insert_order_detail($data)
	{
		$this->db->insert('order_detail', $data);
	}

	/**
	 * Update stok product by id and quantity
	 * @param  int $product_id 
	 * @param  int $qty       
	 * @return void             
	 */
	public function update_product_stock($product_id, $qty)
	{
		$this->db->set('stock', 'stock-'.$qty, FALSE);
		$this->db->where('serial', $product_id);
		$this->db->update('products');
	}

	/**
	 * Update product by Id
	 * @param  int $product_id 
	 * @param  array $data      
	 * @return void            
	 */
	public function update_product($product_id, $data)
	{	
		$this->db->where('serial', $product_id);
		$this->db->update('products', $data);
	}

	/**
	 * Insert Product
	 * @param  array $data 
	 * @return int       
	 */
	public function insert_product($data)
	{
		$this->db->insert('products', $data);
		$id = $this->db->insert_id();
		return (isset($id)) ? $id : FALSE;
	}

	/**
	 * Get all product with pagination
	 * @param  int $number 
	 * @param  int $offset 
	 * @return array         
	 */
	public function get_all_products_pagination($number, $offset)
	{
		return $query = $this->db->get('products', $number, $offset)->result_array();
	}

	/**
	 * Delete product by id
	 * @param  int $product_id 
	 * @return void            
	 */
	public function delete_product($product_id)
	{
		$this->db->where('serial', $product_id);
		$this->db->delete('products');
	}

	/**
	 * Get count of all product
	 * @return int 
	 */
	public function get_count_products()
	{
		return $this->db->get('products')->num_rows();
	}

	/**
	 * Get product by Id
	 * @param  int $product_id 
	 * @return object             
	 */
	public function get_product($product_id)
	{
		return $this->db->where('serial', $product_id)->get('products')->row();
	}

	/**
	 * Get User by id
	 * @param  int $user_id
	 * @return object
	 */
	public function get_user($user_id)
	{
		return $this->db->where('id', $user_id)->get('users')->row();
	}

	// get user profile by user id
	/**
	 * Get user profile by user id
	 * @param  int $user_id 
	 * @return object      
	 */
	public function get_user_profile($user_id)
	{
		return $this->db->where('user_id', $user_id)->get('user_profiles')->row();
	}

	// update field of stripe_customer_id from table users
	/**
	 * Update field of stripe_customer_id form tanle
	 * @param  [type] $user_id            [description]
	 * @param  [type] $stripe_customer_id [description]
	 * @return [type]                     [description]
	 */
	public function update_user_stripe_customer_id($user_id, $stripe_customer_id)
	{
		$this->db->set('stripe_customer_id', $stripe_customer_id);
		$this->db->where('id', $user_id);
		$this->db->update('users');
		//$this->db->update('users', array('stripe_customer_id', $stripe_customer_id));  //if not user 'set' in $this->db
	}

	/**
	 * Get all user with pagination
	 * @param  int $user_id 
	 * @param  int $number  
	 * @param  int $offset 
	 * @return array          
	 */
	public function get_all_user_orders_pagination($user_id, $number, $offset)
	{
		return $query = $this->db->where('user_id', $user_id)->get('orders', $number, $offset)->result_array();
	}

	/**
	 * Get count user order
	 * @param  int $user_id 
	 * @return int          
	 */
	public function get_count_user_orders($user_id)
	{
		return $this->db->where('user_id', $user_id)->get('orders')->num_rows();
	}

	/**
	 * Get all user with pagination
	 * @param  int $number 
	 * @param  int $offset
	 * @return array         
	 */
	public function get_all_orders_pagination( $number, $offset)
	{
		return $query = $this->db->get('orders', $number, $offset)->result_array();
	}

	/**
	 * Get count user order
	 * @return int
	 */
	public function get_count_orders()
	{
		return $this->db->get('orders')->num_rows();
	}

	/**
	 * Get total order detail by order id
	 * @param  int $order_id 
	 * @return object          
	 */
	public function get_total_item_order($order_id)
	{
		return $this->db->select('SUM(quantity*price) as total')->where('orderid', $order_id)->get('order_detail')->row();
	}

	/**
	 * Get all users data with pagination
	 * @param  int $number 
	 * @param  int $offset 
	 * @return array         
	 */
	public function get_all_users_pagination($number, $offset)
	{
		return $query = $this->db->get('users', $number, $offset)->result_array();
	}

	// get count users
	/**
	 * Get Count users
	 * @return int 
	 */
	public function get_count_users()
	{
		return $this->db->get('users')->num_rows();
	}
       
}